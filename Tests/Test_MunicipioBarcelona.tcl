source "Test_Conf.tcl"
source "../RegionShape.tcl"
source "../GeoGraph.tcl"

set objMUN [ RegionShape %AUTO% \
              -shpfile "$DataPath/SpainMaps/esp_muni_00" \
              -idfield "PROVMUN" \
              -namefield "MUNICIPIO0" \
              -level "Municipios" ]

set cod_muni 08019
set name_muni [ $objMUN GetEntityName 08019 ]
puts "El municipio $cod_muni es $name_muni"

set geometry [ $objMUN GetEntityGeometry $cod_muni ]

puts "La geometria de $name_muni tiene [ llength $geometry ] contornos"

set g [ GeoGraph .g ]
pack $g -fill both -expand yes

update 

blt::busy hold $g

$g AddRegions $objMUN

#set cod_muni "all"
set lay1 [ $g AddLayer "Municipios" \
               -label "Municipios" \
               -entities $cod_muni -color \#c0ff00 -outline gray50 ]
$g DrawLayer $lay1
update
blt::busy release $g
$g ZoomAll

set test_show 0
if { $test_show} {
  foreach b $geometry {
    $g marker create polygon -coords [ lindex $b 0 ] -fill red
  }
}


