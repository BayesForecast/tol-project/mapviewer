source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"
source "../tcl/GeoGraph.tcl"

set objCOM [ RegionShape %AUTO% \
                 -shpfile [ file join $MapPath "spain_regions_ind"  ]\
                 -idfield "COM" \
                 -namefield "NOMBRE99" \
                 -level "Comunidades" ]

set g [ GeoGraph .g ]
pack $g -fill both -expand yes

$g AddRegionShape $objCOM -toplevel 1
set layer [ $g AddLayer "Comunidades" ]
$g ZoomAll

$g DrawLayer $layer