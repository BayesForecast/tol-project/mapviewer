source "Test_Conf.tcl"
source "../tcl/GeoVariable.tcl"

GeoVariable geo_var

set fd [ open "$DataPath/Variables/Variable1.csv" ]
gets $fd line
gets $fd line
while { $line ne "" } {
  if { ![ regexp "(.+);(.+)%$" $line ==> mun cuota ] } {
    puts "fail processing $line"
  }
  geo_var SetValue $mun $cuota
  gets $fd line
}
close $fd