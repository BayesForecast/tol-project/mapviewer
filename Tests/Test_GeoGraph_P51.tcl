source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"
source "../tcl/GeoGraph.tcl"

set shp "$DataPath/P51/P51"

set obj [ RegionShape %AUTO% \
              -shpfile $shp \
              -idfield "PROVMUN DISTRITO SECCION" \
              -namefield "" \
              -level "SSCC_P51" ]


set g [ GeoGraph .g ]
pack $g -fill both -expand yes

$g AddRegionShape $obj -toplevel 1
set layer [ $g AddLayer "SSCC_P51" ]
$g ZoomAll

$g DrawLayer $layer