source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"
source "../tcl/GeoGraph.tcl"

set shp "$DataPath/P18/P18"

set objPROV [ RegionShape %AUTO% \
              -shpfile "$MapPath/spain_provinces_ind_2" \
              -idfield "PROV" \
              -namefield "NOMBRE99" \
              -level "Provincias" ]

set objSSCC_P18 [ RegionShape %AUTO% \
                      -shpfile $shp \
                      -idfield "PROVMUN DISTRITO SECCION" \
                      -namefield "" \
                      -level "SSCC_P18" ]


set g [ GeoGraph .g ]
pack $g -fill both -expand yes

$g AddRegionShape $objPROV -toplevel 1
$g AddRegionShape $objSSCC_P18 

set layer_P18 [ $g AddLayer "SSCC_P18" -activeborderwidth 2 ]

set layer_PROV [ $g AddLayer "Provincias" \
                     -label "Provincias" -color "" -bordercolor red ]
$g ZoomAll

$g DrawLayer $layer_PROV
$g DrawLayer $layer_P18

$g RaiseLayer $layer_P18