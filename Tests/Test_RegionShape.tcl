source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"

set obj [ RegionShape %AUTO% \
              -shpfile "$DataPath/SpainMaps/spain_regions_ind" \
              -idfield "COM" \
              -namefield "NOMBRE99" \
              -level "Comunidades" ]

puts "[$obj cget -shpfile]"
puts "[$obj GetEntityCodes]"
puts "[$obj GetEntityName CA07]"

set geometry [$obj GetEntityGeometry CA07]
puts "[ llength $geometry ]"
puts "[ llength [ lindex $geometry 0 ] ]"
puts "[ llength [ lindex [ lindex $geometry 0 ] 0 ] ]"
puts [ lindex [ lindex $geometry 0 ] 0 ]
puts "MinMax = [ $obj GetBoundingBox ]"