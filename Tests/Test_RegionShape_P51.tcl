source "Test_Conf.tcl"
package require tclshp

set shp "$DataPath/P51/P51"
puts [dbfinfo $shp]

set ShapesGeom [ shpget $shp ]
set ShapesInfo [ dbfget $shp ]

puts "Shapes = [ llength $ShapesInfo ]"
puts [ lindex $ShapesInfo 0 ]
puts "First shape = [ llength [ lindex $ShapesInfo 0 ] ]"
puts "Second shape = [ llength [ lindex [ lindex $ShapesInfo 0 ] 0 ] ]"

source "../tcl/RegionShape.tcl"

set obj [ RegionShape %AUTO% \
              -shpfile $shp \
              -idfield "PROVMUN DISTRITO SECCION" \
              -namefield "" \
              -level "Comunidades" ]

puts "[$obj cget -shpfile]"
puts "[$obj GetEntityCodes]"

set geometry [$obj GetEntityGeometry 51001:06:001]
puts "[ llength $geometry ]"
puts "[ llength [ lindex $geometry 0 ] ]"
puts "[ llength [ lindex [ lindex $geometry 0 ] 0 ] ]"
puts [ lindex [ lindex $geometry 0 ] 0 ]
puts "MinMax = [ $obj GetBoundingBox ]"