# Tcl package index file, version 1.1

proc LoadTclshp { version dir } {
  global tcl_platform
  
  set suffix [info sharedlibextension]
  if {$tcl_platform(platform) eq "unix"} {
    set name libtclshp[info sharedlibextension]
  } else  {
    set name tclshp.dll
  }
  load [file join $dir $name]
  package provide tclshp 0.1
}

set version "0.1"

package ifneeded tclshp $version [list LoadTclshp $version $dir]
