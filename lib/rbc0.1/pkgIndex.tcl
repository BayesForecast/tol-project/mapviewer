package ifneeded rbc 0.1 "
    # This package always requires Tk
    [list package require Tk]
    [list load [file join $dir rbc01.dll]]
    [list source [file join $dir graph.tcl]]
"
