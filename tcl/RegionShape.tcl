package require snit
package require tclshp

snit::type RegionShape {

  option -shpfile -default "" -readonly 1
  option -idfield -default "" -readonly 1
  option -namefield -default "" -readonly 1
  option -level -default "" -readonly 1
  variable data -array {
    minmax ""
  }

  constructor { args } {
    $self configurelist $args
    if { $options(-shpfile) ne "" } {
      $self _ReadData
      $self _ProcessData
    }
  }

  destructor {
  }

  method GetNumberOfRecords { } {
    return [ llength $data(dbf) ]
  }

  method GetEntityCodes { } {
    return $data(entity,codes)
  }

  method GetEntityGeometry { id } {
    return $data(entity,geom,$id)
  }

  method GetEntityName { id } {
    return $data(entity,name,$id)
  }
  method SetEntityGeometry { id name geometry } {
    lappend data(entity,geom,$id) $geometry
    if { [ llength $data(entity,geom,$id) ] == 1 } {
      lappend data(entity,codes) $id
      set data(entity,name,$id) $name
    }
  }

  method GetBoundingBox { } {
    if { [llength $data(minmax)] } {
      return $data(minmax)
    }
    # based on shpinfo
    foreach {xmin ymin xmax ymax} [lrange $data(shpinfo) 2 end] break;
    set data(minmax) [ list $xmin $xmax $ymin $ymax ]
    return $data(minmax)

    # previous code not based on shpinfo
    set entities [ $self GetEntityCodes ]
    set geom0 [ $self GetEntityGeometry [ lindex $entities 0 ] ]
    foreach {xmin ymin} [ lindex [ lindex $geom0 0 ] 0 ] break
    set xmax $xmin
    set ymax $ymin
    foreach e $entities {
      foreach boundary [ $self GetEntityGeometry $e ] {
        foreach { x y } [ lindex $boundary 0 ] {
          if { $x < $xmin } {
            set xmin $x
          } elseif { $x > $xmax } {
            set xmax $x
          }
          if { $y < $ymin } {
            set ymin $y
          } elseif { $y > $ymax } {
            set ymax $y
          }
        }
      }
    }
    set data(minmax) [ list $xmin $xmax $ymin $ymax ]
    return $data(minmax)
  }

  method DumpHeader { } {
    puts "DBF structure"
    puts $data(dbfinfo)
    puts "SHP info"
    puts $data(shpinfo)
  }
  
  method DumpEntityInfo { index } {
    puts [ lindex $data(dbf) $index ]
  }

  method _ReadData { } {
    set data(dbfinfo) [ dbfinfo $options(-shpfile) ]
    set data(dbf) [ dbfget $options(-shpfile) ]
    set data(shpinfo) [shpinfo $options(-shpfile)]
    # read all polygons
    set data(shp) [ shpget $options(-shpfile) ]
  }

  method _GetFieldNames { } {
    set all_fields [ list ]
    foreach f $data(dbfinfo) {
      lappend all_fields [ lindex $f 0 ]
    }
    return $all_fields
  }

  method GetRecord { idx_rec } {
    return [ lindex $data(dbf) $idx_rec ]
  }

  method GetFieldValue { idx_rec f } {
    set idx [ $self _GetFieldIndex $f ]
    return [ lindex [ lindex $data(dbf) $idx_rec ] $idx ]
  }

  method _GetFieldIndex { f } {
    return $data(dbf,field,$f)
  }

  method _ProcessHeader { } {
    set i 0
    foreach f [ lrange $data(dbfinfo) 1 end ] {
      set data(dbf,field,[ lindex $f 0]) $i
      incr i
    }
  }

  method _ProcessData { } {
    $self _ProcessHeader
    if { $options(-idfield) eq "" } {
      error "there is no id field defined"
    }
    set idxs_id {}
    foreach id $options(-idfield) {
      set idx [ lsearch -regexp $data(dbfinfo) ^$id ]
      if { $idx == -1 } {
        error "id field $options(-idfield) not found in [$self _GetFieldNames]"
      }
      incr idx -1
      lappend idxs_id $idx
    }
    if { $options(-namefield) ne "" } {
      set idx_name [ lsearch -regexp $data(dbfinfo) ^$options(-namefield) ]
      if { $idx_name == -1 } {
        error "name field $options(-namefield) not found in [$self _GetFieldNames]"
      }
      incr idx_name -1
    } else {
      set idx_name -1
    }
    foreach dbf $data(dbf) geom $data(shp) {
      set id_entity {}
      foreach idx $idxs_id {
        set id [ lindex $dbf $idx ]
        if { $id eq "" } {
          set id_entity UNKNOWN
          break
        }
        lappend id_entity $id
      }
      if { $id_entity ne "UNKNOWN" } {
        set id_entity [ join $id_entity ":" ]
      }
      if { $idx_name == -1 } {
        set name_entity "UNNAMED"
      } else {
        set name_entity [ lindex $dbf $idx_name ]
      }
      # geom is a list of polygons, each polygon is a list of rings
      $self SetEntityGeometry $id_entity $name_entity $geom
    }
    #puts $data(dbfinfo)
    #puts [ lindex $data(dbf) 4 ]
  }
}
