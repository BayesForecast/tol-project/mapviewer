package require snit

snit::type RangeTable {

  variable Table ""
  
  constructor { args } {
  }

  destructor {
  }

  method GetSize { } {
    return [ expr [ llength $Table ] - 1 ]
  }

  method GetTable { } {
    return $Table
  }

  method BuildTable { _type args } {
    set Table {}
    #puts "$self BuildTable $_type [list $args]"
    if { $_type eq "nquantiles" } {
      array set opts $args
      $self _BuildTableNQuantile $opts(-values) $opts(-size)
    } elseif { $_type eq "quantiles" } {
      array set opts $args
      $self _BuildTableQuantiles $opts(-values) $opts(-prob)
    } elseif { $_type eq "uniform" } {
      array set opts {
        -values ""
        -minmax ""
      }
      array set opts $args
      if { $opts(-values) eq "" } {
        $self _BuildTableUniformFromBounds \
            [ lindex $opts(-minmax) 0 ] [ lindex $opts(-minmax) 1 ] $opts(-size)
      } else {
       $self _BuildTableUniformFromValues $opts(-values) $opts(-size)
      }
    } elseif { $_type eq "absolute" } {
      array set opts $args
      $self _BuildTableAbsolute $opts(-values)
    } else {
      error "BuildTable: unknown type $_type, must be nquantiles, quantiles, uniform or absolute"
    } 
  }

  method _BuildTableUniformFromBounds { min max size } {
    set Table [ list $min ]
    set step [ expr { ( $max - $min ) / ( double( $size ) + 1 ) } ]
    for { set i 1 } { $i < $size } { incr i } {
      set v [ expr { $min + $i * $step } ]
      lappend Table $v
    }
    lappend Table $max
  }

  method _BuildTableUniformFromValues { values size } {
    set min [ lindex $values 0 ]
    set max $min
    foreach v $values {
      if { $v < $min } {
        set min $v
      } elseif { $v > $max } {
        set max $v
      }
      $self _BuildTableUniformFromBounds $min $max $size
    }
  }

  method _BuildTableAbsolute { values } {
    set Table [ lsort -real $values ]
  }

  method _BuildTableNQuantile { values size } {
    set N [ llength $values ]
    if { !$size || $size >= $N } {
      error "BuildTable: invalid size $size"
    }
    set prob {}
    for { set i 0 } { $i <= $size } { incr i } {
      lappend prob [ expr { double($i) / double($size) } ]
    }
    return [ $self _BuildTableQuantiles $values $prob ]
  }

  method _BuildTableQuantiles { values prob } {
    set N [ llength $values ]
    set _values [ lsort -real $values ]
    set prob [ lsort -real $prob ]
    if { [ lindex $prob 0 ] > 0.0 } {
      set prob [ linsert $prob 0 0.0 ]
    }
    if { [ lindex $prob end ] < 1.0 } {
      lappend prob 1.0
    }
    set N [ llength $_values ]
    incr N -1
    foreach p $prob {
      set _i [ expr { $p * $N } ]
      set i [ expr { int( $_i ) } ]
      set f [ expr { $_i - $i } ]
      if { $f == 0.0 } {
        set x [ lindex $_values $i ]
      } else {
        set x0 [ lindex $_values $i ]
        incr i
        set x1 [ lindex $_values $i ]
        set x [ expr { $x0 + $f * ( $x1 - $x0 ) } ]
      }
      lappend Table $x
    }
  }

  method FindInterval { v } {
    return [ _BinSrch0 $Table $v ]
  }

  proc _BinSrch0 { lst x } {
    set N [ llength $lst ]
    if { !$N } {
      return -1
    }
    return [ _BinSrch1 $lst $x 0 [ incr N -1 ] ]
  }

  proc _BinSrch1 { lst x low up } {
    if { [ expr { $up - $low } ] == 1 } {
      return $low
    } else {
      set pivotIndex [expr { ( $low + $up ) / 2 } ]
      set pivotValue [ lindex $lst $pivotIndex ]
      if { $x < $pivotValue } {
        return [ _BinSrch1 $lst $x $low $pivotIndex ]
      } else {
        _BinSrch1 $lst $x $pivotIndex $up
      }
    }
  }
}