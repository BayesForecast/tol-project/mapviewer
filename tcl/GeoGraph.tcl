package require Tk
package require snit

source [ file join [ file dir [ info script ] ] "RGB.tcl" ]
source [ file join [ file dir [ info script ] ] "RangeTable.tcl" ]

# BEGIN: https://www.tol-project.org/ticket/1655
if {[info exist ::toltk_script_path]} {
  namespace eval :: source [list [file join $::toltk_script_path "graph.tcl"]]
}
# END: https://www.tol-project.org/ticket/1655

snit::widgetadaptor GeoGraph {

  variable Position
  variable BoundingBox
  variable Limits
  variable State
  variable Variables -array {}
  variable CurrentVariable ""
  variable RegionShapes
  variable Locations
  variable Layers
  variable LookupTable -array {}
  variable LayerInfo -array {
    idcounter 0
  }
  variable ActiveEntity -array {
    id ""
    regions ""
    layer ""
  }
  
  delegate option * to hull
  delegate method * to hull
  
  proc Busy { what args } {
    puts "-- Busy $what $args --"
    switch $what {
      patched {
        return 1
      }
      default {
        
      }
    }
  }

  proc PopZoom { graph } {
    global zoomInfo

    puts "GeoGraph::PopZoom"
    if {[catch {$graph info type}]} {
      # $graph is not of tyoe GeoGraph
      return [_blt_PopZoom0 $graph]
    }

    # this code is specific to GeoGraph
   set zoomStack $zoomInfo($graph,stack)
    if { [llength $zoomStack] > 0 } {
      set listLimits [lindex $zoomStack 0]
      array set lim $listLimits
      set zoomInfo($graph,stack) [lrange $zoomStack 1 end]
      set w [$graph extents plotwidth]
      set h [$graph extents plotheight]
      set aspectV [expr {double($h)/$w}]
      foreach {xmin ymin xmax ymax} \
          [$graph fixAspectRatio $aspectV $lim(-xmin) $lim(-ymin) $lim(-xmax) $lim(-ymax)] break;
      foreach v {xmin ymin xmax ymax} {
        set axis [string index $v 0]
        set opt [string range $v 1 end]
        $graph axis configure $axis -$opt [set $v]
      }
      eval $graph _SetLimits $listLimits
    }
  }

  proc PushZoom { graph } {
    global zoomInfo

    puts "GeoGraph::PushZoom"
    if {[catch {$graph info type}]} {
      # $graph is not of tyoe GeoGraph
      return [_blt_PushZoom0 $graph]
    }
    # specific code for GeoGraph
    eval $graph marker delete [$graph marker names "zoom*"]
    if { [info exists zoomInfo($graph,afterId)] } {
	after cancel $zoomInfo($graph,afterId)
    }
    set x1 $zoomInfo($graph,A,x)
    set y1 $zoomInfo($graph,A,y)
    set x2 $zoomInfo($graph,B,x)
    set y2 $zoomInfo($graph,B,y)

    if { ($x1 == $x2) || ($y1 == $y2) } { 
      # No delta, revert to start
      return
    }
    set xmin [$graph axis invtransform x $x1]
    set xmax [$graph axis invtransform x $x2]
    set ymin [$graph axis invtransform y $y1]
    set ymax [$graph axis invtransform y $y2]
    if { $xmin > $xmax } {
      foreach {xmin xmax} [list $xmax $xmin] break
    } 
    if { $ymin > $ymax } {
      foreach {ymin ymax} [list $ymax $ymin] break
    }
    set ::zoomInfo($graph,stack) \
        [linsert $::zoomInfo($graph,stack) 0 [$graph _GetLimits]]
    # store current limits
    $graph _SetLimits -xmin $xmin -xmax $xmax -ymin $ymin -ymax $ymax
    set w [$graph extents plotwidth]
    set h [$graph extents plotheight]
    set aspectV [expr {double($h)/$w}]
    foreach {xmin ymin xmax ymax} \
        [$graph fixAspectRatio $aspectV $xmin $ymin $xmax $ymax] break;
    # make current limits appears non-distorted
    foreach v {xmin ymin xmax ymax} {
      set axis [string index $v 0]
      set opt [string range $v 1 end]
      $graph axis configure $axis -$opt [set $v]
    }
  }
  
  typeconstructor {
    package require Tk
    set use_rbc 0
    if { $use_rbc } {
      package require rbc
      interp alias {} _Graph {} rbc::graph
      interp alias {} _ZoomStack {} Rbc_ZoomStack
      interp alias {} _Crosshairs {} Rbc_Crosshairs
    } else {
      package require -exact BLT 2.4
      interp alias {} _Graph {} blt::graph
      interp alias {} _ZoomStack {} Blt_ZoomStack
      interp alias {} _Crosshairs {} Blt_Crosshairs
    }
    _Graph .__g__
    _ZoomStack .__g__
    destroy .__g__

    if { $use_rbc } {
      if { ![ llength [ info command ::rbc::_PopZoom ] ] } {
        rename ::rbc::PopZoom ::rbc::_PopZoom
        interp alias {} _rbc_PopZoom0 {} ::rbc::_PopZoom
        #rename ::MyPopZoom ::rbc::PopZoom 
        rename PopZoom ::rbc::PopZoom 
        
        rename ::rbc::PushZoom ::rbc::_PushZoom
        interp alias {} _rbc_PushZoom0 {} ::rbc::_PushZoom
        #rename ::MyPushZoom ::rbc::PushZoom
        rename PushZoom ::rbc::PushZoom
        #interp alias {} _busy {} ::rbc::busy
        # https://www.tol-project.org/ticket/1344
        if { ![ llength [ info commands _rbc_busy ] ] } {
          rename ::rbc::busy _rbc_busy
          interp alias {} ::rbc::busy {} Busy
        }
      }
    } elseif { ![ llength [ info command ::blt::_PopZoom ] ] } {
      rename ::blt::PopZoom ::blt::_PopZoom
      interp alias {} _blt_PopZoom0 {} ::blt::_PopZoom
      #rename ::MyPopZoom ::blt::PopZoom 
      rename PopZoom ::blt::PopZoom 
      
      rename ::blt::PushZoom ::blt::_PushZoom
      interp alias {} _blt_PushZoom0 {} ::blt::_PushZoom
      #rename ::MyPushZoom ::blt::PushZoom
      rename PushZoom ::blt::PushZoom
      #interp alias {} _busy {} ::blt::busy
      # https://www.tol-project.org/ticket/1344
      if { [ catch { blt::busy patched } ] } {
        rename ::blt::busy _blt_busy
        interp alias {} ::blt::busy {} [ myproc Busy ]
        puts "interp alias {} ::blt::busy {} [ myproc Busy ]"
      }
    }
  }

  constructor { args } {

    installhull using _Graph
    canvas $win.clegend -background white
    _Crosshairs $win
    _ZoomStack $win
    bind $win <Configure> [ mymethod _OnConfigure %w %h ]
    bind $win <Motion> [mymethod _OnMotion %x %y]
    #$win marker bind region <Motion> [mymethod _OnMotion %x %y]
    $self configurelist $args
  }
  
  destructor {
  }

  # must keep the aspect ratio

  method _OnConfigure { w h } {
    if { $w == 0 || $h == 0 || ![info exists Limits(-xmin)] } {
      return
    }
    set leftmargin   [$win extents leftmargin]
    set rightmargin  [$win extents rightmargin]
    set topmargin    [$win extents topmargin]
    set bottommargin [$win extents bottommargin]
    set aspectV [expr {double($h-$topmargin-$bottommargin)/($w-$leftmargin-$rightmargin)}]
    foreach {xmin ymin xmax ymax} \
        [$self fixAspectRatio $aspectV $Limits(-xmin) $Limits(-ymin) $Limits(-xmax) $Limits(-ymax)] break;
    $win axis configure x -min $xmin -max $xmax
    $win axis configure y -min $ymin -max $ymax    
  }

  method fixAspectRatio { aspectV xmin ymin xmax ymax } {
    set dx0 [expr {double($xmax - $xmin)}]
    set dy0 [expr {double($ymax - $ymin)}]
    set aspectW [expr {$dy0/$dx0}]
    set cx  [expr {0.5 * ($xmax + $xmin)}]
    if {$aspectW < $aspectV} {
      # should grow in y direction
      set dy_2 [expr {0.5*($dy0 + $dx0*($aspectV-$aspectW))}]
      set cy  [expr {0.5 * ($ymax + $ymin)}]
      set ymin [expr {$cy - $dy_2}]
      set ymax [expr {$cy + $dy_2}]
    } elseif {$aspectW > $aspectV} {
      #should grow in x direction
      set dx_2 [expr {0.5*($dx0 + $dy0*(1/$aspectV-1/$aspectW))}]
      set cx  [expr {0.5 * ($xmax + $xmin)}]
      set xmin [expr {$cx - $dx_2}]
      set xmax [expr {$cx + $dx_2}]
    } else {
      # viewport and world has the same aspect ratio
    }
    return [list $xmin $ymin $xmax $ymax]
  }

  method _OnConfigure0 { w h } {
    if { $w == 0 || $h == 0 || ![ info exists Limits(xmin) ] } {
      return
    }
    foreach v { xmin ymin xmax ymax } {
      set $v $Limits($v)
    }
    set dx0 [expr {1.0 * ($xmax - $xmin)}]
    set dy0 [expr {1.0 * ($ymax - $ymin)}]
    set cx  [expr {0.5 * ($xmax + $xmin)}]
    set cy  [expr {0.5 * ($ymax + $ymin)}]
    if { $w <= $h } {
      set dy [expr $dx0*double($h)/double($w)]
      if { $dy < $dy0 } {
        set r [ expr { $dy0 / $dy } ]
        set dx [ expr { $dx0 * $r } ]
        set dy [ expr { $dy * $r } ]
      } else {
        set dx $dx0
      }
    } else {
      set dx [ expr $dy0*double($w)/double($h) ]
      if { $dx < $dx0 } {
        set r [ expr { $dx0 / $dx } ]
        set dx [ expr { $dx * $r } ]
        set dy [ expr { $dy0 * $r } ]
      } else {
        set dy $dy0
      }
    }
    set hdx [ expr { $dx/2.0 } ]
    set hdy [ expr { $dy/2.0 } ]
    set xmin [ expr { $cx - $hdx } ]
    set xmax [ expr { $cx + $hdx } ]
    set ymin [ expr { $cy - $hdy } ]
    set ymax [ expr { $cy + $hdy } ]
    $win axis configure x -min $xmin -max $xmax
    $win axis configure y -min $ymin -max $ymax
  }

  method busy { what } {
    Busy $what $win
  }

  # add a set of polygonal regions
  method AddRegionShape { obj args } {
    set region_level [ $obj cget -level ]
    if { $region_level eq "" } {
      error "Could not add region '$obj' because its -level is empty"
    }
    array set opts {
      -toplevel 0
    }
    array set opts $args
    set RegionShapes($region_level) $obj
    if { $opts(-toplevel) } {
      foreach {x0 x1 y0 y1} [ $obj GetBoundingBox ] break
      array set BoundingBox [list -xmin $x0 -xmax $x1 -ymin $y0 -ymax $y1]
    }
  }

  method ZoomAll { } {
    if { ![ info exists BoundingBox ] } {
      set x0 ""
      foreach level [ $self GetRegionLevels ] {
        set objRegion [ $self GetRegionShape $level ]
        foreach { _x0 _x1 _y0 _y1 } [ $objRegion GetBoundingBox ] break
        if { $x0 eq "" } {
          set x0 $_x0
          set x1 $_x1
          set y0 $_y0
          set y1 $_y1
        } else {
          if { $x0 < $_x0 } {
            set x0 $_x0
          } elseif { $x1 < $_x1 } {
            set x1 $_x1
          }
          if { $y0 < $_y0 } {
            set y0 $_y0
          } elseif { $y1 < $_y1 } {
            set y1 $_y1
          }
        }
      }
      array set BoundingBox [list -xmin $x0 -xmax $x1 -ymin $y0 -ymax $y1]
    }
    # update the limits to fit the bounding box
    eval $self _SetLimits [array get BoundingBox]
    # render the map keeping aspect ratio
    $self _OnConfigure [winfo width $win] [winfo height $win]
  }

  method UpdateLimitsFromGraph { } {
    foreach a { x y } {
      foreach o { min max } {
        set Limits($a$o) [ $win axis cget $a -$o ]
      }
    }
  }

  method _GetLimits {} {
    array get Limits
  }

  method _SetLimits {args} {
    array set Limits $args
  }
  
  method GetRegionShape { level } {
    return $RegionShapes($level)
  }

  method GetRegionLevels { } {
    return [ array names RegionShapes ]
  }

  method AddLocations { obj } {
  }
  
  method UpdateLegend { } {
    set c $win.clegend
    set ctable [$self GetColorTable]
    if {$ctable ne ""} {
      set ranges [$self GetRangeTable]
      RGB::UpdateLegendCanvas $c $ctable -maxsize 10 \
          -values [$ranges GetTable] -fontsize [$self GetLegendFontSize] \
          -labels [$self GetLegendLabels] -maxsize [$self GetLegendSize]
      set w [$c cget -width]
      set h [$c cget -height]
      set offsetX [expr {-[$win extents rightmargin]-5}]
      set offsetY [expr {-[$win extents bottommargin]-5}]
      place $c -in $win -anchor se -relx 1.0 -rely 1.0 -x $offsetX -y $offsetY
    }
  }

  method AddVariable { var args } {
    set regionlevel [ $var cget -regionlevel ]
    set name [ $var cget -name ]
    lappend Variables($regionlevel,$name) $var
    array set opts {
      -rangedef { uniform 10 }
      -palette { gradient \#eff3ff \#08519c }
      -minmax ""
      -legendfontsize 0
      -legendlabels {}
      -legendsize 10
    }
    array set opts $args

    ### process range definition

    set ranges [ RangeTable %AUTO% ]
    set rangeType [ lindex $opts(-rangedef) 0 ]
    if { $rangeType eq "absolute" } {
      $ranges BuildTable "absolute" -values [ lindex $opts(-rangedef) 1 ]
    } elseif { $rangeType eq "nquantiles" } {
      $ranges BuildTable "nquantiles" \
          -values [ $var GetRawValues ] -size [ lindex $opts(-rangedef) 1 ]
    } elseif { $rangeType eq "quantiles" } {
      $ranges BuildTable "nquantiles" \
          -values [ $var GetRawValues ] -prob [ lindex $opts(-rangedef) 1 ]
    } elseif { $rangeType eq "uniform" } {
      if { $opts(-minmax) ne "" } {
        $ranges BuildTable "uniform" \
            -minmax $opts(-minmax) -size $opts(-size)        
      } else {
        $ranges BuildTable "uniform" \
            -values [ $var GetRawValues ] -size [ lindex $opts(-rangedef) 1 ]
      }
    } else {
      error "AddVariable: unknown range type '$rangeType'"
    }
    set LookupTable($name,ranges) $ranges
    
    ### process color definition
    set paletteType [ lindex $opts(-palette) 0 ]
    
    if { $paletteType eq "gradient" } {
      set LookupTable($name,ctable) [ RGB::Gradient %AUTO% \
                                          -from [ lindex $opts(-palette) 1 ] \
                                          -to [ lindex $opts(-palette) 2 ] \
                                          -size [ $ranges GetSize ] ]
    } elseif { $paletteType eq "palette" } {
      foreach {from to} [ RGB::GetPalette [ lindex $opts(-palette) 1 ] ] break;
      set LookupTable($name,ctable) [ RGB::Gradient %AUTO% \
                                          -from $from -to $to \
                                          -size [ $ranges GetSize ] ]
    } elseif { $paletteType eq "table" } {
      set LookupTable($name,ctable) [ RGB::Table %AUTO% ]
      $LookupTable($name,ctable) SetColors [ lindex  $opts(-palette) 1 ]
    } else {
      error "AddVariable: unknown palette type $paletteType"
    }
    set LookupTable($name,legendfontsize) $opts(-legendfontsize)
    set LookupTable($name,legendlabels) $opts(-legendlabels)    
    set LookupTable($name,legendsize) $opts(-legendsize)    
  }

  # define and draw a layer, 
  # args:
  #   -label
  #   -bordercolor "" | color
  #   -borderwidth 1
  #   -activebordercolor "" | color (TODO)
  #   -activeborderwidth 1 (TODO)
  #   -color "" | choose | random | rgb | {variable cuota}
  #   -entities { e1, e2, ..., ek } | all
  method AddLayer { region_level args } {
    array set opts {
      -bordercolor gray50
      -borderwidth 1
      -activebordercolor black
      -activeborderwidth 1
      -color choose
      -missingcolor white
      -entities all
      -label ""
    }
    if { ![ info exists RegionShapes($region_level) ] } {
      error "Could not add new layer because region level $region_level does not exists"
    }
    array set opts $args
    set layer [ $self _NewLayerName ]
    set LayerInfo($layer,level) $region_level
    set LayerInfo($layer,entities) $opts(-entities)
    set LayerInfo($layer,bordercolor) $opts(-bordercolor)
    set LayerInfo($layer,borderwidth) $opts(-borderwidth)
    set LayerInfo($layer,activebordercolor) $opts(-activebordercolor)
    set LayerInfo($layer,activeborderwidth) $opts(-activeborderwidth)
    set LayerInfo($layer,missingcolor) $opts(-missingcolor)
    if { $opts(-color) eq "choose" } {
      set LayerInfo($layer,color)  ""
    } elseif { [ lindex $opts(-color) 0 ] eq "variable" } {
      set LayerInfo($layer,color) "variable"
      set LayerInfo($layer,color,variable) [ lindex $opts(-color) 1 ]
    } else {
      set LayerInfo($layer,color) $opts(-color)
    }
    return $layer
  }

  method _NewLayerName { } {
    incr LayerInfo(idcounter)
    return "Layer_$LayerInfo(idcounter)"
  }

  method RaiseLayer { layer } {
    foreach m [ $win marker names "${layer}__*" ] { 
      $win marker before $m
    }
  }

  method GetColorTable { } {
    if {$CurrentVariable ne ""} {
      return $LookupTable($CurrentVariable,ctable)
    }
    return ""
  }

  method GetRangeTable { } {
    if {$CurrentVariable ne ""} {
      return $LookupTable($CurrentVariable,ranges)
    }
    return ""
  }

  method GetLegendFontSize { } {
    if {$CurrentVariable ne ""} {
      return $LookupTable($CurrentVariable,legendfontsize)
    }
    return 0
  }

  method GetLegendSize { } {
    if {$CurrentVariable ne ""} {
      return $LookupTable($CurrentVariable,legendsize)
    }
    return 10
  }

  method GetLegendLabels { } {
    if {$CurrentVariable ne ""} {
      return $LookupTable($CurrentVariable,legendlabels)
    }
    return ""
  }

  method DrawLayer { layer } {
    set level $LayerInfo($layer,level)
    set bordercolor $LayerInfo($layer,bordercolor)
    set borderwidth $LayerInfo($layer,borderwidth)
    set prefix "${layer}__"
    #set color [ GetRandomRGB ]
    set colordef  $LayerInfo($layer,color)

    set objRegion [ $self GetRegionShape $level ]
    if { $LayerInfo($layer,entities) eq "all" } {
      set entities [ $objRegion GetEntityCodes ]
    } else {
      set entities $LayerInfo($layer,entities)
    }
    if { $colordef eq "variable" } {
      set var_name $LayerInfo($layer,color,variable)
      set CurrentVariable $var_name
      set var_obj  $Variables($level,$var_name)
      set ranges   $LookupTable($var_name,ranges)
      set ctable $LookupTable($var_name,ctable)
    }
    foreach id $entities {
      set geomId 0
      foreach polygon [ $objRegion GetEntityGeometry $id ] {
        set draw 1
        if { $colordef eq "variable" } {
          if { [ catch { $var_obj GetValue $id } v ] } {
            set v ""
            set color ""
            set draw 0
          } else {
            if { $v eq "?" } {
              set color $LayerInfo($layer,missingcolor)
            } else {
              set color [ $ctable GetColor \
                              [ $ranges FindInterval $v ] ]
            }
          }
        } else {
          set color $colordef
        }
        if { $draw } {
         foreach ring $polygon {
           incr geomId
           set markerId ${prefix}${id}_$geomId
           $win marker create polygon \
               -name $markerId \
               -bindtags region \
               -coords $ring \
               -fill $color -outline $bordercolor -linewidth $borderwidth
          }
        }
      }
    }
  }

  method GetActiveEntity { } {
    return $ActiveEntity(id)
  }

  method GetActiveLayer { } {
    return $ActiveEntity(layer)
  }

  method GetActiveRegionLevel { } {
    set layer [ $self GetActiveLayer ]
    if { $layer ne "" } {
      return $LayerInfo($layer,level)
    } else {
      return ""
    }
  }

  method GetActiveRegionShape { } {
    set level [ $self GetActiveRegionLevel ]
    if { $level ne "" } {
      return [ $self GetRegionShape $level ]
    }
    return ""
  }

  method GetActiveVariableValue { } {
    set layer [ $self GetActiveLayer ]
    if { $layer ne "" && $LayerInfo($layer,color) eq "variable" } {
      set var_name $LayerInfo($layer,color,variable)
      set level $LayerInfo($layer,level)
      set var_obj  $Variables($level,$var_name)
      return [ $var_obj GetValue $ActiveEntity(id) ]
    } else {
      return ""
    }
  }

  method GetLastPosition { {system world} } {
    return $Position($system)
  }

  method _OnMotion { x y } {
    set Position(world) [$win invtransform $x $y]
    set Position(viewport) [list $x $y]
    event generate $win <<CursorMove>>
    set over [ $win marker find overlapping \
                   [expr {$x-1}] [expr {$y-1}] [expr {$x+1}] [expr {$y+1}] ]
    if { [ llength $over ] } {
      set item [ lindex $over 0 ]
      if { [ regexp ^(Layer_.+)__(.+)_ $over ==> layer id ] } {
        if { $id ne $ActiveEntity(id) } {
          set over_regions [ $win marker names ${layer}__${id}_* ]
          if { $ActiveEntity(id) ne "" } {
            set activeLayer $ActiveEntity(layer)
            foreach p $ActiveEntity(regions) {
              $win marker configure $p \
                  -outline $LayerInfo($activeLayer,bordercolor) \
                  -linewidth $LayerInfo($activeLayer,borderwidth)
            }
          }
          set ActiveEntity(id) $id
          set ActiveEntity(layer) $layer
          set ActiveEntity(regions) $over_regions
          foreach p $over_regions {
            $win marker before $p
            $win marker configure $p \
                -outline $LayerInfo($layer,activebordercolor) \
                -linewidth $LayerInfo($layer,activeborderwidth)
          }
          event generate $win <<ActivateEntity>>
        }
        set level $LayerInfo($layer,level)
      }
    } else {
      if { $ActiveEntity(id) ne "" } {
        set activeLayer $ActiveEntity(layer)
        foreach p $ActiveEntity(regions) {
          $win marker configure $p \
              -outline $LayerInfo($activeLayer,bordercolor) \
              -linewidth $LayerInfo($activeLayer,borderwidth)
        }
      }
      set ActiveEntity(id) ""
    }
  }  
}


