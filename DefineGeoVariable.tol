//////////////////////////////////////////////////////////////////////////////
// FILE:  def_geo_variable.tol
// PURPOSE:
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
Class @GeoVariable
//////////////////////////////////////////////////////////////////////////////
{
  Text _.name;        // Nombre de la variable
  Text _.desc;        // descripcion de la variable
  Set _.parentLevel; // Region padre, es util para delimitar regiones
                      // vacias (donde no hay datos). Se define como
                      // un par (con nombre) [[Text map, Text level]],
                      // donde map es el nombre del NameBlock que
                      // contiene la definicion del mapa y level es el
                      // nivel dentro de dicho mapa.
  Set _.regionLevel; // Nivel de regin Shape para el que est�
                      // definida la variable. Estos valores los ha
                      // definido el paquete. Es un conjunto similar
                      // al documentado en _.parentLevel
  Set _.values;       // Valores: Es un conjunto indexado de Reales donde el 
     // nombre de cada elemento se corresponde el identificador de una entidad
     // geom�trica. Los valores son datos reales que correponden al valor que
     // toma el elemento para esa variable.


  ////////////////////////////////////////////////////////////////////////////
  // Forma de representar la variable.
  ////////////////////////////////////////////////////////////////////////////
  // Para representar una variable se van a dar dos cosas:
  //   1. Los intervalos en los que se agrupan los valores de la variable y
  Set _.rangeDef;
  //   2. los colores con los que se pintan los elementos que est�n en cada
  //   uno de esos intervalos.
  Set _.colorDef;
  // 
  // 1. INTERVALOS:
  //  Existen dos formas de especificar los intervalos en los que se agrupan
  // los posibles valores de una variable 
  //   a) Cuantiles
  //     En este caso se especifica la cantidad de cuantiles en los que se
  //    quiere dividir la variable. Sea este n�mero n. Entonces se cumple que
  //    cada intervalo tiene 1/n elementos.
  // 
  //   b) Intervalaci�n
  //     En este caso se van a dar rangos de valores. Hay dos formas de dar 
  //     rangos:
  //     1) N intervalos iguales. 
  //       En este caso se especifica el valor m�nimo que podr�a tomar la 
  //     variable, el valor m�ximo y la cantidad de intervalos
  //     (Habr� una funci�n que alerte de valores de la variable por debajo
  //     del valor m�nimo o por encima del valor m�ximo)
  // 
  //     2) N intervalos especificos
  //       SetOfSet(
  //         [[Real iniInt1, Real finInt1]],  
  //         [[Real iniInt2, Real finInt2]],  
  //         .. 
  //         [[Real iniIntN, Real finIntN]]
  //       )
  //     (Habr� una funci�n que compruebe que cada intervalo empieza en
  //     el mismo n�mero donde termina el anterior, que no hay valores 
  //     inferiores al inicio del intervalo 1 y que el valor fin del �ltimo
  //     intervalo es +inf)
  // Al final esto queda as�:

  // divisi�n uniforme en 10 intervalos desde el min al max de los datos
  // Set _.rangeDef = { [[ Text type = "Uniform", Real size = 10 ]] };

  // divisi�n seg�n n cuantiles equidistantes:
  // Set _.rangeDef = { [[ Text type = "NQuantile", Real size = 10 ]] };
  
  // divisi�n seg�n n cuantiles dados:
  // Set _.rangeDef = { [[ Text type = "Quantiles", Set prob = [[ 0, 0.25, 0.75, 1 ]] };

  // divisi�n seg�n un particionamiento dado:
  // Set _.rangeDef = { [[ Text type = "Absolute", Set points = [[ min,...,max ]] } ;
  // el intervalo i-esimo dado por points[i]<= v<  points[i+1]



  // 
  // 2. COLORES
  // Para las dos formas de especificar variables el resultado final es un 
  // conjunto de n intervalos donde cada intervalo es representado con un color.
  //  Por tanto, aparte de la especificaci�n de la intervalaci�n (que dar�a 
  // siempre un conjutno de n intervalaciones) se da un conjunto de colores de
  // la misma cardinalidad de forma que el el color i-�simo corresponde al 
  // i-�simo intervalo

  // Para los distintos casos en los que se dan "n colores" existe una forma
  // de pedir a una paleta esos colores.

  // Al final esto queda asi:

  // Para los colores podemos usar un conjunto que defina el esquema de
  // colores, por ejemplo:
  // Set _.colorDef = { [[ Text type = "Gradient", Set definition = [[Text from = "green", Text to = "red" ]] } ;

  // tambi�n pudi�ramos darlo de esta forma
  // Set _.colorDef = { [[ Text type = "Gradient", Text palette = "error"]] } ;
  // esto har�a uso de una paleta de colores asociada al nombre "error", usada
  // para plotear errores del modelo. El gradiente genera tantos colores como
  // intervalos se hayan pedido o especificado.

  // otra forma posible,

  // Set _.colorDef = { [[ Text type = "Table", Set colors = [[ "red", "green", "blue" ]] ]] } ;

  // esta �ltima forma debe contener tantos colores como intervalos hayamos
  // espcificado.

  // El componente gr�fico se encarga de recibir esa informaci�n y visualizar
  // calculando lo que haga falta.

  Set _.legendDef = Copy(Empty);

  Set SetColorDef( Set colorDef )
  {
    _.colorDef := colorDef
  };

  Set SetRangeDef( Set rangeDef )
  {
    _.rangeDef := rangeDef
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @GeoVariable New(NameBlock args)
  ////////////////////////////////////////////////////////////////////////////
  {

    @GeoVariable obj = [[
      Text _.name         = args::_.name;
      Text _.desc      = getOptArg( args, "_.desc", "" );
      Set _.parentLevel  = getOptArg( args, "_.parentLevel", Copy(Empty) );
      Set _.regionLevel  = args::_.regionLevel;
      Set _.values        = args::_.values;
      Set _.rangeDef      = args::_.rangeDef;
      Set _.colorDef      = args::_.colorDef;
      Set _.legendDef  = getOptArg( args, "_.legendDef", Copy(Empty) )
    ]]
  }
};
